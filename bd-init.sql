-- --------------------------------------------------------
-- Hôte:                         127.0.0.1
-- Version du serveur:           5.6.50 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Listage de la structure de la table cfcloanmanagerprod. echeancier
DROP TABLE IF EXISTS `echeancier`;
CREATE TABLE IF NOT EXISTS `echeancier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rang` int(11) NOT NULL,
  `expected_date` date NOT NULL,
  `real_date` date DEFAULT NULL,
  `expected_amount` decimal(21,2) NOT NULL,
  `real_amount` decimal(21,2) DEFAULT NULL,
  `demand_date` date DEFAULT NULL,
  `observations` longtext,
  `created_date` datetime,
  `updated_date` datetime,
  `engagement_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_echeancier_engagement_id` (`engagement_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Listage des données de la table cfcloanmanagerprod.echeancier : 0 rows
/*!40000 ALTER TABLE `echeancier` DISABLE KEYS */;
/*!40000 ALTER TABLE `echeancier` ENABLE KEYS */;

-- Listage de la structure de la table cfcloanmanagerprod. echeancier_suivi
DROP TABLE IF EXISTS `echeancier_suivi`;
CREATE TABLE IF NOT EXISTS `echeancier_suivi` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(20) NOT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `jhi_order` int(11) NOT NULL,
  `color` varchar(255) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `operation` varchar(255) DEFAULT NULL,
  `description` longtext,
  `created_date` datetime,
  `updated_date` datetime,
  `alert_interval` int(11) DEFAULT NULL,
  `nb_max_alert` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Listage des données de la table cfcloanmanagerprod.echeancier_suivi : 0 rows
/*!40000 ALTER TABLE `echeancier_suivi` DISABLE KEYS */;
INSERT INTO `echeancier_suivi` (`id`, `identifier`, `libelle`, `jhi_order`, `color`, `value`, `operation`, `description`, `created_date`, `updated_date`, `alert_interval`, `nb_max_alert`) VALUES
	(1, 'VERT', 'Note d\'information', 1, 'bg-success', -42, '<=', 'Date du jour≥ 06 semaines de la date prévisionnelle de déblocage (vert) ', '2022-08-03 23:00:00', '2022-08-03 23:00:00', 3, 3),
	(2, 'JAUNE', 'Note d\'information', 2, 'bg-warning', -28, '<=', '06 semaines ≥Date du jour≥ 04 semaines de la date prévisionnelle de déblocage (jaune)', '2022-08-03 23:00:00', '2022-08-03 23:00:00', NULL, NULL),
	(3, 'VIOLET', 'Note d\'information', 3, 'bg-purple', -14, '<=', '04 semaines ≥Date du jour≥ 02 semaines de la date prévisionnelle de déblocage ', '2022-08-03 23:00:00', '2022-08-03 23:00:00', NULL, NULL),
	(4, 'ROUGE', 'Lettre et visite de chantier', 4, 'bg-danger', -14, '>', 'Date du jour ≤ 02 semaines de la date prévisionnelle de déblocage - Rouge', '2022-08-03 23:00:00', '2022-08-03 23:00:00', NULL, NULL);
/*!40000 ALTER TABLE `echeancier_suivi` ENABLE KEYS */;

-- Listage de la structure de la table cfcloanmanagerprod. echeancier_suivi_action
DROP TABLE IF EXISTS `echeancier_suivi_action`;
CREATE TABLE IF NOT EXISTS `echeancier_suivi_action` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `jhi_order` int(11) NOT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `description` longtext,
  `notifier_client` bit(1) DEFAULT NULL,
  `sms` varchar(255) DEFAULT NULL,
  `responsable` varchar(255) DEFAULT NULL,
  `created_date` datetime,
  `updated_date` datetime,
  `echeanciersuivi_id` bigint(20) NOT NULL,
  `notifier_bet` bit(1) DEFAULT NULL,
  `alert_level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_echeancier_suivi_action_echeanciersuivi_id` (`echeanciersuivi_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- Listage des données de la table cfcloanmanagerprod.echeancier_suivi_action : 0 rows
/*!40000 ALTER TABLE `echeancier_suivi_action` DISABLE KEYS */;
INSERT INTO `echeancier_suivi_action` (`id`, `jhi_order`, `libelle`, `description`, `notifier_client`, `sms`, `responsable`, `created_date`, `updated_date`, `echeanciersuivi_id`, `notifier_bet`, `alert_level`) VALUES
	(11, 12, 'Faire une descente inopinée sur le chantier ', 'Faire une descente inopinée sur le chantier ', b'0', NULL, 'ROLE_ANALYSTE_AGENCE', '2022-09-01 23:00:00', '2022-09-01 23:00:00', 4, b'0', 2),
	(12, 13, 'Adresse une lettre formelle au client et/ou le BET pour lui rappeler la date prévisionnelle du Nième déblocage.', 'Adresse une lettre formelle au client et/ou le BET pour lui rappeler la date prévisionnelle du Nième déblocage. L’invite à prendre des dispositions pour atteindre le niveau d’exécution souhaité et dépose sa demande de déblocage au plus tard dans (01) une semaine ;', b'1', NULL, 'ROLE_ANALYSTE_AGENCE', '2022-09-01 23:00:00', '2022-09-01 23:00:00', 4, b'1', -1),
	(13, 14, 'Faire le point à l’analyste au siège ', 'Faire le point à l’analyste au siège ', b'0', NULL, 'ROLE_ANALYSTE_AGENCE', '2022-09-01 23:00:00', '2022-09-01 23:00:00', 4, b'0', 2),
	(14, 15, 'Adresse une lettre formelle S/c DADC et DCR au Chef/Directeur d’agence', 'Adresse une lettre formelle S/c DADC et DCR au Chef/Directeur d’agence, avec DCPC en copie, pour lui rappeler que la date prévisionnelle du dépôt de la Nième déblocage devrait être déposé en agence dans une (01) semaine et par conséquent l’invite à fournir toutes les informations nécessaire au sujet de ce prêt.', b'0', NULL, 'ROLE_ANALYSTE_SIEGE', '2022-09-01 23:00:00', '2022-09-01 23:00:00', 4, b'0', 2),
	(8, 8, 'Relance le client et/ou le BET à travers un message téléphonique ou Email pour lui rappeler que les informations sur l’état d’avancement de ses travaux du chantier', 'Relance le client et/ou le BET à travers un message téléphonique ou Email pour lui rappeler que les informations sur l’état d’avancement de ses travaux du chantier sont attendues et l’invite à prendre des dispositions pour que lesdits travaux puissent atteindre le niveau d’exécution souhaité et dépose sa demande de déblocage au plus tard dans (02) deux semaines.', b'1', NULL, 'ROLE_ANALYSTE_AGENCE', '2022-09-01 23:00:00', '2022-09-01 23:00:00', 3, b'1', 2),
	(9, 9, 'faire le point à l’analyste au siège', 'faire le point à l’analyste au siège', b'0', NULL, 'ROLE_ANALYSTE_AGENCE', '2022-09-01 23:00:00', '2022-09-01 23:00:00', 3, b'0', 1),
	(10, 10, 'Relance l’analyste en l’agence  via lotus pour lui rappeler qu’il n’avait pas eu de retour', 'Relance l’analyste en l’agence  via lotus pour lui rappeler qu’il n’avait pas eu de retour sur des informations sur l’état d’avancement des travaux du chantier.', b'0', NULL, 'ROLE_ANALYSTE_SIEGE', '2022-09-01 23:00:00', '2022-09-01 23:00:00', 3, b'0', 1),
	(6, 6, 'faire le point à l’analyste au siège', 'faire le point à l’analyste au siège', b'0', NULL, 'ROLE_ANALYSTE_AGENCE', '2022-09-01 23:00:00', '2022-09-01 23:00:00', 2, b'0', 1),
	(5, 5, 'Adresse un message téléphonique ou Email au client et/ou le BET ', 'Adresse un message téléphonique ou Email au client et/ou le BET pour lui rappeler de la date prévisionnelle du Nième déblocage et l’invite à fournir des informations sur l’état d’avancement des travaux du chantier.', b'1', 'Cher client la date prévisionnel de la RANG ème tranche de votre prêt est le DATEPREV. Prière de fournir les informations sur l\'état d\'avancement des travaux de votre chantier.', 'ROLE_ANALYSTE_AGENCE', '2022-09-01 23:00:00', '2022-09-01 23:00:00', 2, b'1', 2),
	(7, 7, 'Adresse un message via lotus à l’analyste en agence concernée', 'Adresse un message via lotus à l’analyste en agence concernée pour lui rappeler de la date prévisionnelle du Nième déblocage et l’invite à lui fournir des informations sur l’état d’avancement des travaux du chantier.', b'0', NULL, 'ROLE_ANALYSTE_SIEGE', '2022-09-01 23:00:00', '2022-09-01 23:00:00', 2, b'0', 1),
	(1, 1, 'Etablir un programme de descente sur le terrain ', 'Etablir un programme de descente sur le terrain ', b'0', '', 'ROLE_ANALYSTE_AGENCE', '2022-08-03 23:00:00', '2022-08-03 23:00:00', 1, b'0', -1),
	(3, 3, 'Etablir un rapport + perspectives sur le projet', 'Etablir un rapport + perspectives sur le projet', b'0', NULL, 'ROLE_ANALYSTE_AGENCE', '2022-09-01 23:00:00', '2022-09-01 23:00:00', 1, b'0', 1),
	(2, 2, 'Etablir un chronogramme de communication avec le client et/ou BET ', 'Etablir un chronogramme de communication avec le client et/ou BET en charge du projet pour le maintien de la veille des échéances ', b'0', NULL, 'ROLE_ANALYSTE_AGENCE', '2022-08-03 23:00:00', '2022-08-03 23:00:00', 1, b'0', 2),
	(4, 3, 'Validation des chronogrammes, programmes et rapports de l’analyste en agence ', 'Validation des chronogrammes, programmes et rapports de l’analyste en agence ', b'0', NULL, 'ROLE_ANALYSTE_SIEGE', '2022-09-01 23:00:00', '2022-09-01 23:00:00', 1, b'0', -1);
/*!40000 ALTER TABLE `echeancier_suivi_action` ENABLE KEYS */;

-- Listage de la structure de la table cfcloanmanagerprod. entite
DROP TABLE IF EXISTS `entite`;
CREATE TABLE IF NOT EXISTS `entite` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(32) NOT NULL,
  `label` varchar(255) NOT NULL,
  `short_label` varchar(50) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `is_enabled` bit(1) NOT NULL,
  `adrs_1` varchar(255) DEFAULT NULL,
  `adrs_2` varchar(255) DEFAULT NULL,
  `adrs_3` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `decision` decimal(21,2) DEFAULT NULL,
  `parameters` longtext,
  `type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_entite_identifier` (`identifier`),
  KEY `fk_entite_type_id` (`type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- Listage des données de la table cfcloanmanagerprod.entite : 0 rows
/*!40000 ALTER TABLE `entite` DISABLE KEYS */;
INSERT INTO `entite` (`id`, `identifier`, `label`, `short_label`, `parent`, `is_enabled`, `adrs_1`, `adrs_2`, `adrs_3`, `zipcode`, `city`, `country`, `email`, `decision`, `parameters`, `type_id`) VALUES
	(1, 'DCR', 'Direction du Crédit', 'DCR', NULL, b'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
	(2, 'CFC/101', 'Agence de Yaoundé', 'AYDE', 1, b'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
	(3, 'CFC/102', 'Agence de Douala', 'ADLA', 1, b'1', 'Rue DUWARF BONANJO, face CCA Bank', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
	(4, 'CFC/103', 'Agence de Garoua', 'AGRA', 1, b'1', 'Avenue des banques', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
	(5, 'CFC/104', 'Agence de Bamenda', 'ABDA', 1, b'1', 'Chemist Round About', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
	(6, 'CFC/105', 'Agence de Bafoussam', 'ABFA', 1, b'1', 'AVENUE TAMDJA, face MTN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
	(7, 'CFC/106', 'Agence de Bertoua', 'ABTA', 1, b'1', 'A proximité de la Communauté Urbaine', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
	(8, 'CFC/107', 'Agence d\'Ebolowa', 'AEBW', 1, b'1', 'Carrefour AN 2000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
	(9, 'CFC/108', 'Agence de Maroua', 'AMRA', 1, b'1', 'Avenue des Banques, face Afriland First Bank', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
	(10, 'CFC/109', 'Agence de Buea', 'ABUEA', 1, b'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
	(11, 'CFC/110', 'Agence de Ngaoundéré', 'ANDERE', 1, b'1', 'Face Cathédrale notre Dame des apôtres', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
	(12, 'DADC', 'Département de l\'Administration des Crédits', 'DADC', 1, b'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3),
	(13, 'SDEYDE', 'SOUS-DIRECTION-EXPLOITATION YAOUNDE', 'SDEYDE', 2, b'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3),
	(14, 'SAPYDE', 'Service de l\'Analyse des Prêts Agence de Yaoundé', 'SAPYDE', 13, b'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4);
/*!40000 ALTER TABLE `entite` ENABLE KEYS */;

-- Listage de la structure de la table cfcloanmanagerprod. jhi_authority
DROP TABLE IF EXISTS `jhi_authority`;
CREATE TABLE IF NOT EXISTS `jhi_authority` (
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Listage des données de la table cfcloanmanagerprod.jhi_authority : 9 rows
/*!40000 ALTER TABLE `jhi_authority` DISABLE KEYS */;
INSERT INTO `jhi_authority` (`name`) VALUES
	('ROLE_ADMIN'),
	('ROLE_ANALYSTE'),
	('ROLE_ANALYSTE_AGENCE'),
	('ROLE_ANALYSTE_SIEGE'),
	('ROLE_CHARGE_CLIENTELE'),
	('ROLE_CHEF_AGENCE'),
	('ROLE_DADC'),
	('ROLE_DCR'),
	('ROLE_DIRECTEUR'),
	('ROLE_MANAGER'),
	('ROLE_SDE'),
	('ROLE_SOUS_DIRECTEUR'),
	('ROLE_USER');
/*!40000 ALTER TABLE `jhi_authority` ENABLE KEYS */;

-- Listage de la structure de la table cfcloanmanagerprod. parameters
DROP TABLE IF EXISTS `parameters`;
CREATE TABLE IF NOT EXISTS `parameters` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `param_value_string` varchar(255) DEFAULT NULL,
  `param_value_int` int(11) DEFAULT NULL,
  `param_value_date` datetime,
  `param_value_json` longtext,
  `updated_date` datetime,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Listage des données de la table cfcloanmanagerprod.parameters : 0 rows
/*!40000 ALTER TABLE `parameters` DISABLE KEYS */;
INSERT INTO `parameters` (`id`, `identifier`, `description`, `param_value_string`, `param_value_int`, `param_value_date`, `param_value_json`, `updated_date`) VALUES
	(1, 'default_status_id', 'Status par défaut', NULL, 1, NULL, NULL, NULL),
	(2, 'period_unit', 'unité de mesure de la durée', 'jour(s)', NULL, NULL, NULL, NULL),
	(3, 'sms', 'paramètre des SMS', NULL, NULL, NULL, '{"active":true}', NULL),
	(4, 'send_mail_to_client', NULL, NULL, 1, NULL, NULL, NULL),
	(5, 'notification_list', 'Liste des personne à notifier pour une decision ou validation', 'fouetieus@yahoo.fr,fouetieus@gmail.com', NULL, NULL, NULL, NULL),
	(6, 'ALERT_ROLE', 'Rôle à alerter', 'ROLE_DCR;ROLE_DADC;ROLE_ANALYSTE_SIEGE', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `parameters` ENABLE KEYS */;

-- Listage de la structure de la table cfcloanmanagerprod. product
DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_product_code` (`code`),
  UNIQUE KEY `ux_product_label` (`label`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Listage des données de la table cfcloanmanagerprod.product : 0 rows
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Listage de la structure de la table cfcloanmanagerprod. type_entite
DROP TABLE IF EXISTS `type_entite`;
CREATE TABLE IF NOT EXISTS `type_entite` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(32) NOT NULL,
  `label` varchar(255) NOT NULL,
  `short_label` varchar(50) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `parameters` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_type_entite_identifier` (`identifier`),
  UNIQUE KEY `ux_type_entite_label` (`label`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Listage des données de la table cfcloanmanagerprod.type_entite : 0 rows
/*!40000 ALTER TABLE `type_entite` DISABLE KEYS */;
INSERT INTO `type_entite` (`id`, `identifier`, `label`, `short_label`, `icon`, `parameters`) VALUES
	(1, 'DIR', 'DIRECTION', 'DIR', NULL, NULL),
	(2, 'AG', 'AGENCE', 'AGENCE', NULL, NULL),
	(3, 'SDIR', 'SOUS-DIRECTION', 'SDIR', NULL, NULL),
	(4, 'SRV', 'SERVICE', 'SRV', NULL, NULL);
/*!40000 ALTER TABLE `type_entite` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;

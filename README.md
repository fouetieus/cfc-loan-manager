MISE EN PROD DE L'APPLICATION CCMLOANMANAGER VIA DOCKER

#PREREQUIS
INSTALLATION DE DOCKER (DESKTOP ou NATIF) SUR LA MACHINE DE PROD 

#MISE EN PROD
- Cloner le fichier app.yml (docker compose file)
git clone https://gitlab.com/fouetieus/cfc-loan-manager.git
cd cfc-loan-manager

-EXECUTER LE fichier
docker compose / docker-compose -f app.yml up

-Attendez que les packets se téléchargent et que les images s'exécutent en suite lancez l'application sur le port 8080
